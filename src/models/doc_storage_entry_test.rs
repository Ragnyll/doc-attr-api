#[cfg(test)]
mod tests {
    use std::path::{PathBuf};
    use crate::models::doc_storage_entry::StoredDocument;

    #[test]
    fn test_new() {
        StoredDocument::new(
            PathBuf::from("/foo/bar/txt.txt"),
            vec![String::from("test_tag")],
        );
    }

    #[test]
    fn test_get_tags() {
        let stored_document = StoredDocument::new(
            PathBuf::from("/foo/bar/txt.txt"),
            vec![String::from("test_tag")],
        );

        let doc_tags = stored_document.get_tags().to_vec();
        assert_eq!(doc_tags[0], String::from("test_tag"));
    }


    #[test]
    fn test_get_document_path() {
        let stored_document = StoredDocument::new(
            PathBuf::from("/foo/bar/txt.txt"),
            vec![String::from("test_tag")],
        );

        assert_eq!(*stored_document.get_document_path(), PathBuf::from("/foo/bar/txt.txt"));
    }
}
