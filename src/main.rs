#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

mod models;
// use models::doc_storage_entry::StoredDocument;

mod api;
use api::document;


fn main() {
    rocket::ignite()
        .mount(
            "/document",
            routes![
                document::retrieve_all,
                document::document_by_id,
                document::document_by_query,
                document::download_document,
            ],
        )
        .launch();
}
