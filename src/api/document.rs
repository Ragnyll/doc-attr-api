use crate::models::doc_storage_entry::StoredDocument;
use std::path::PathBuf;

use rocket::request::{Form};
use rocket_contrib::json::Json;

#[derive(FromForm)]
pub struct TagQuery {
    contains_tags: String,
    omits_tags: Option<String>,
}

#[get("/")]
pub fn retrieve_all() -> Json<Vec<StoredDocument>> {
    let doc1 = StoredDocument::new(PathBuf::from("/usr/test"), vec!(String::from("tags")));
    let doc2 = StoredDocument::new(PathBuf::from("/usr/test2"), vec!(String::from("tags")));
    let docs = vec!(doc1, doc2);

    Json(docs)
}

#[get("/<document_id>")]
pub fn document_by_id(document_id: String) -> String {
    format!("Retreiving document by id {}", document_id)
}

#[get("/tags?<query..>")]
pub fn document_by_query(query: Option<Form<TagQuery>>) -> String {
    if let Some(query) = query {
        if let Some(omits_tags) = &query.omits_tags {
            format!(
                "running query, omits_tags: {} and contains_tags: {}",
                omits_tags, query.contains_tags
            )
        } else {
            format!("running query contains_tags: {}!", query.contains_tags)
        }
    } else {
        "Query must contain at least one tag".into()
    }
}

#[get("/download/<document_id>")]
pub fn download_document(document_id: String) -> String {
    format!("Downloading document by id {}", document_id)
}
