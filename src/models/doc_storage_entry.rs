use std::path::PathBuf;
use serde::Serialize;

#[derive(Debug)]
#[derive(Serialize)]
pub struct StoredDocument {
    document_path: PathBuf,
    tags: Vec<String>,
}

impl StoredDocument {
    pub fn new(document_path: PathBuf, tags: Vec<String>) -> StoredDocument {
        return StoredDocument {
            document_path: document_path,
            tags: tags,
        };
    }

    pub fn get_document_path(&self) -> &PathBuf {
        &self.document_path
    }

    pub fn get_tags(&self) -> &Vec<String> {
        &self.tags
    }
}
