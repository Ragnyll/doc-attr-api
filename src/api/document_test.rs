#[cfg(test)]
mod tests {
    use crate::document;
    use crate::rocket;
    use rocket::local::Client;

    fn client() -> Client {
        Client::new(rocket::ignite().mount(
            "/",
            routes![document::retrieve_all, document::document_by_id, document::document_by_query],
        ))
        .unwrap()
    }

    fn test_uri(uri: &str, expected: String) {
        let client = client();
        assert_eq!(client.get(uri).dispatch().body_string(), Some(expected));
    }

    #[test]
    fn test_retrieve_all() {
        test_uri("/", String::from("Retrieving all documents"))
    }

    #[test]
    fn test_document_by_id() {
        test_uri(
            &format!("/{}", "1234"),
            format!("Retreiving document by id {}", "1234"),
        )
    }

    #[test]
    fn test_document_by_query_contains() {
        test_uri(
            &format!("/tags?contains_tags={}", "dogs"),
            format!("running query contains_tags: {}!", "dogs"),
        );
    }

    #[test]
    fn test_document_by_query_contains_and_omits() {
        test_uri(
            &format!("/tags?omits_tags={}&contains_tags={}", "cats", "dogs"),
            format!("running query, omits_tags: {} and contains_tags: {}", "cats", "dogs"),
        );
    }

    #[test]
    fn test_document_by_query_omits() {
        // should 400. uri requires a contains
        // TODO: figure out how to 400
    }
}
