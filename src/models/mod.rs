pub mod doc_storage_entry;

#[cfg(test)]
#[path = "./doc_storage_entry_test.rs"]
mod doc_storage_entry_test;
